package com.example.textcalculator;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    String firstNumber;
    String secondNumber;
    String result;
    EditText editTextOne;
    EditText editTextTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextOne = findViewById(R.id.first);
        editTextTwo = findViewById(R.id.second);
    }

    public void onClickButton(View view) {
        firstNumber = editTextOne.getText().toString();
        secondNumber = editTextTwo.getText().toString();

        result = firstNumber + secondNumber;

        Intent intent = new Intent(MainActivity.this, ItselfCalculator.class);
        intent.putExtra("firstNumber", firstNumber);
        intent.putExtra("secondNumber", secondNumber);
        intent.putExtra("result", result);
        startActivity(intent);
    }
}