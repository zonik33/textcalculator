package com.example.textcalculator;

import android.content.Intent;
import android.os.Bundle;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class ItselfCalculator extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = new TextView(this);

        Intent intent = getIntent();
        String firstNumber = intent.getStringExtra("firstNumber");
        String secondNumber = intent.getStringExtra("secondNumber");
        String result = intent.getStringExtra("result");
        String text = firstNumber + "+" + secondNumber + "=" + result;
        textView.setText(text);
    }
}

